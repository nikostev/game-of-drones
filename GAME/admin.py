from django.contrib import admin
from GAME.models import *

# Register your models here.
admin.site.register(Player)
admin.site.register(Game)
admin.site.register(Round)