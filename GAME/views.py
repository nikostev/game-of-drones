from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from rest_framework.response import Response
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view
from rest_framework import status
from .models import *
from rest_framework.decorators import parser_classes
from rest_framework.parsers import JSONParser

@api_view(['POST'])
@parser_classes((JSONParser,))
def initgame(request):
	try:
		if request.method == 'POST':
			namePlayerOne = request.data["playerOne"]
			namePlayerTwo = request.data["playerTwo"]

			playerOne = None
			playerTwo = None

			try:
				playerOne = Player.objects.get(C_Name=namePlayerOne.strip())
			except Player.DoesNotExist:
				playerOne = Player()
				playerOne.C_Name = namePlayerOne.strip()
				playerOne.save()

			try:
				playerTwo = Player.objects.get(C_Name=namePlayerTwo.strip())
			except Player.DoesNotExist:
				playerTwo = Player()
				playerTwo.C_Name = namePlayerTwo.strip()
				playerTwo.save()

			game = Game()
			game.FK_player1 = playerOne
			game.FK_player2 = playerTwo
			game.save()

			json=[]
			DirtTEM = {}
			DirtTEM["playerOneId"] = playerOne.id
			DirtTEM["playerTwoId"] = playerTwo.id
			DirtTEM["gameId"] = game.id
			json.append(DirtTEM)

			return Response({"data": json,"status": status.HTTP_200_OK})

	except:
		return Response({'data': [], "status": status.HTTP_400_BAD_REQUEST})


def checkMove(game,player,move):
	roundGame = None
	lastRound = Round.objects.filter(FK_Game=game,C_RoundWinner='ND').order_by('id')

	if lastRound.exists():
		roundGame = lastRound[0]
		if (game.FK_player2 == player):
			roundGame.C_MoveP2 = move
			roundGame.save()
		else:
			return "it's not player 1's turn", roundGame
	else:
		roundGame = Round()
		if (game.FK_player1 == player):
			roundGame.C_MoveP1 = move 
			roundGame.FK_Game = game
			roundGame.save()
		else:
			return "it's not player 2's turn", roundGame
	return "successful", roundGame

def checkRound(roundGame):
	if (roundGame.C_MoveP1!='ND' and roundGame.C_MoveP2!='ND'):
		if (roundGame.C_MoveP1 == roundGame.C_MoveP2):
			roundGame.C_RoundWinner = 'DW'
		elif roundGame.C_MoveP1 == "R" and roundGame.C_MoveP2 == "S":
			roundGame.C_RoundWinner = 'P1'
		elif roundGame.C_MoveP1 == "P" and roundGame.C_MoveP2 == "R":
			roundGame.C_RoundWinner = 'P1'
		elif roundGame.C_MoveP1 == "S" and roundGame.C_MoveP2 == "P":
			roundGame.C_RoundWinner = 'P1'
		else:
			roundGame.C_RoundWinner = 'P2'
		roundGame.save()
	return roundGame

def checkWinner(game):
	winnerCountP1 = Round.objects.filter(FK_Game=game,C_RoundWinner='P1').count()
	winnerCountP2 = Round.objects.filter(FK_Game=game,C_RoundWinner='P2').count()

	if (winnerCountP1 >= 3):
		game.C_Winner = 'P1'
		game.save()
	elif (winnerCountP2 >= 3):
		game.C_Winner = 'P2'
		game.save()
		
	return game

@api_view(['POST'])
@parser_classes((JSONParser,))
def play(request):
	try:
		if request.method == 'POST':
			playerId = request.data["playerId"]
			gameId = request.data["gameId"]
			move = request.data["move"]

			game = None
			player = None
			roundGame = None

			try:
				game = Game.objects.get(id=gameId)
				if (game.C_Winner!="ND"):
					return Response({'message': ("%s won" %(game.C_Winner)), "status": status.HTTP_200_OK})
			except (Game.DoesNotExist,ValueError):
				return Response({'message': "Game does not exist", "status": status.HTTP_500_INTERNAL_SERVER_ERROR})

			try:
				player = Player.objects.get(id=playerId)
			except (Player.DoesNotExist,ValueError) :
				return Response({'message': "Player does not exist", "status": status.HTTP_500_INTERNAL_SERVER_ERROR})

			message, roundGame = checkMove(game,player,move.upper())

			if (message!="successful"):
				 return Response({'message': message, "status": status.HTTP_200_OK})

			roundGame = checkRound(roundGame)
			game = checkWinner(game)

			json=[]
			DirtTEM = {}
			DirtTEM["gameId"] = game.id
			DirtTEM["roundGameId"] = roundGame.id
			DirtTEM["roundWinner"] = roundGame.C_RoundWinner
			DirtTEM["winner"] = game.C_Winner

			json.append(DirtTEM)

			return Response({"data": json,"status": status.HTTP_200_OK})
	except:
		return Response({'data': [], "status": status.HTTP_400_BAD_REQUEST})

@api_view(['GET'])
@parser_classes((JSONParser,))
def gamesPerPlayer(request):
	try:
		if request.method == 'GET':
			json=[]
			players = Player.objects.all()
			for player in players:

				total = 0;
				total += Game.objects.filter(FK_player1=player,C_Winner='P1').count()
				total += Game.objects.filter(FK_player2=player,C_Winner='P2').count()
				
				DirtTEM = {}
				DirtTEM["player"] = player.C_Name
				DirtTEM["total"] = total
				json.append(DirtTEM)

			return Response({"data": json,"status": status.HTTP_200_OK})

	except:
		return Response({'message': "Internal server error", "status": status.HTTP_500_INTERNAL_SERVER_ERROR})


@api_view(['GET'])
@parser_classes((JSONParser,))
def roundsWonByPlayer(request):
	try:
		if request.method == 'GET':
			json=[]
			players = Player.objects.all()
			for player in players:

				total = 0;
				total += Round.objects.filter(FK_Game__FK_player1=player,C_RoundWinner='P1').count()
				total += Round.objects.filter(FK_Game__FK_player2=player,C_RoundWinner='P2').count()
				
				DirtTEM = {}
				DirtTEM["player"] = player.C_Name
				DirtTEM["total"] = total
				json.append(DirtTEM)

			return Response({"data": json,"status": status.HTTP_200_OK})

	except:
		return Response({'message': "Internal server error", "status": status.HTTP_500_INTERNAL_SERVER_ERROR})
