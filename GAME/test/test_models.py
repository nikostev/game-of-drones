from GAME.models import *
from django.test import TestCase
from django.db import models


class PlayerModelTest(TestCase):

	@classmethod
	def setUpPlayer(cls):
		player = Player.objects.create(C_Name='TestPlayer')
		player.save()
		return player

	def setUp(self):
		pass
	
	
	def test_create_player(self):
		Playerobj= self.setUpPlayer()
		expected_object_name = '%s' %Playerobj.C_Name
		self.assertTrue(isinstance(Playerobj, Player))
		self.assertEquals(expected_object_name,str(Playerobj))
		
class GameModelTest(TestCase):

	@classmethod
	def setUpTestGame(cls):
		player1 = Player.objects.create(C_Name='TestPlayer')
		player1.save()
		player2 = Player.objects.create(C_Name='TestPlayer2')
		player2.save()
		game = Game.objects.create(FK_player1=player1,FK_player2=player2,C_Winner='ND')
		game.save()
		return game

	def test_create_game(self):
		gameobj=self.setUpTestGame()
		expected_object_name = '%s VS %s' % (gameobj.FK_player1.C_Name,gameobj.FK_player2.C_Name)
		self.assertEquals(expected_object_name,str(gameobj))
		self.assertTrue(isinstance(gameobj, Game))

class RoundModelTest(TestCase):

	@classmethod
	def setUpTestRound(cls):
		player1 = Player.objects.create(C_Name='TestPlayer')
		player1.save()
		player2 = Player.objects.create(C_Name='TestPlayer2')
		player2.save()
		game = Game.objects.create(FK_player1=player1,FK_player2=player2,C_Winner='ND')
		game.save()
		Roundobj=Round.objects.create(FK_Game=game,C_MoveP1='ND',C_MoveP2='ND',C_RoundWinner='ND')
		Roundobj.save()
		return Roundobj

	def test_create_round(self):
		Roundobj=self.setUpTestRound()
		expected_object_name = '%s  VS %s' % (Roundobj.C_MoveP1, Roundobj.C_MoveP2)
		self.assertEquals(expected_object_name,str(Roundobj))
		self.assertTrue(isinstance(Roundobj, Round))


	
