from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from GAME.models import *

class AccountTests(APITestCase):


	@classmethod
	def setUpTestGame(cls):
		player1 = Player.objects.create(C_Name='TestPlayer')
		player1.save()
		player2 = Player.objects.create(C_Name='TestPlayer2')
		player2.save()
		game = Game.objects.create(FK_player1=player1,FK_player2=player2,C_Winner='ND')
		game.save()
		return game

	@classmethod
	def setUpTestGameWinner(cls):
		player1 = Player.objects.create(C_Name='TestPlayer')
		player1.save()
		player2 = Player.objects.create(C_Name='TestPlayer2')
		player2.save()
		game = Game.objects.create(FK_player1=player1,FK_player2=player2,C_Winner='P1')
		game.save()
		return game


	def test_init_game(self):
		url = reverse('game')
		data = {"playerOne": "nicoly","playerTwo": "jas"}
		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Game.objects.count(), 1)
		self.assertEqual(Game.objects.get().FK_player1.C_Name, 'nicoly')

	def test_init_game_error(self):
		url = reverse('game')
		data = {}
		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)

	def test_play_game(self):


		self.setUpTestGame()
		url = reverse('play')
		data = {"playerId": 1,"gameId": 1,"move": "R"}
		data2 = {"playerId": 2,"gameId": 1,"move": "P"}

		dataD1 = {"playerId": 1,"gameId": 1,"move": "P"}
		dataD2= {"playerId": 2,"gameId": 1,"move": "P"}

		dataD3 = {"playerId": 1,"gameId": 1,"move": "R"}
		dataD4= {"playerId": 2,"gameId": 1,"move": "S"}

		dataD5 = {"playerId": 1,"gameId": 1,"move": "P"}
		dataD6= {"playerId": 2,"gameId": 1,"move": "R"}


		dataD7 = {"playerId": 1,"gameId": 1,"move": "S"}
		dataD8= {"playerId": 2,"gameId": 1,"move": "P"}

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 1)
		self.assertEqual(Round.objects.get().FK_Game.id, 1)

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 1)
		self.assertEqual(Round.objects.get().FK_Game.id, 1)

		response = self.client.post(url, data2, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 1)
		self.assertEqual(Round.objects.get().FK_Game.id, 1)


		response = self.client.post(url, data2, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 1)
		self.assertEqual(Round.objects.get().FK_Game.id, 1)

		response = self.client.post(url, dataD1, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 2)


		response = self.client.post(url, dataD2, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 2)


		response = self.client.post(url, dataD3, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 3)


		response = self.client.post(url, dataD4, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 3)

		response = self.client.post(url, dataD5, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 4)


		response = self.client.post(url, dataD6, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 4)


		response = self.client.post(url, dataD7, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 5)


		response = self.client.post(url, dataD8, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)
		self.assertEqual(Round.objects.count(), 5)


		url = reverse('gamesPerPlayer')

		response = self.client.get(url, dataD8, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)

		url = reverse('roundsWonByPlayer')

		response = self.client.get(url, dataD8, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_play_game_error(self):
		url = reverse('play')
		data = {}
		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_play_game_winner_p2(self):


		self.setUpTestGame()

		url = reverse('play')
		data = {"playerId": 2,"gameId": 1,"move": "R"}
		data2 = {"playerId": 1,"gameId": 1,"move": "P"}

		dataD1 = {"playerId": 2,"gameId": 1,"move": "P"}
		dataD2= {"playerId": 1,"gameId": 1,"move": "P"}

		dataD3 = {"playerId": 2,"gameId": 1,"move": "R"}
		dataD4= {"playerId": 1,"gameId": 1,"move": "S"}

		dataD5 = {"playerId": 2,"gameId": 1,"move": "P"}
		dataD6= {"playerId": 1,"gameId": 1,"move": "R"}

		dataD7 = {"playerId": 2,"gameId": 1,"move": "S"}
		dataD8= {"playerId": 1,"gameId": 1,"move": "P"}

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_play_game_error_value(self):


		self.setUpTestGame()

		url = reverse('play')
		data = {"playerId": 2,"gameId": 2,"move": "R"}

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_play_game_winner(self):
		self.setUpTestGameWinner()

		url = reverse('play')
		data = {"playerId": 2,"gameId": 1,"move": "R"}

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)


	def test_play_eror_plyer(self):

		self.setUpTestGame()

		url = reverse('play')
		data = {"playerId": 0,"gameId": 1,"move": "R"}

		response = self.client.post(url, data, format='json')

		self.assertEqual(response.status_code, status.HTTP_200_OK)






	