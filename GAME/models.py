# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.urls import reverse

# Create your models here.

class Player(models.Model):
	C_Name= models.CharField(verbose_name="Name",max_length=255,unique=True)
	D_RegisterDate=models.DateField( verbose_name="Register Date",auto_now_add=True, blank = True, null=True)

	def __str__(self):
		return u"%s" %(self.C_Name)

	class Meta:# pragma: no cover
		verbose_name=u'Player'# pragma: no cover
		verbose_name_plural=u'Players'# pragma: no cover


class Game(models.Model):
	FK_player1 = models.ForeignKey('Player', verbose_name="Player 1",blank=True,null=True,on_delete=models.CASCADE,related_name='Player1')
	FK_player2 = models.ForeignKey('Player', verbose_name="Player 2",blank=True,null=True,on_delete=models.CASCADE,related_name='Player2')
	Choice = (
	('P1', 'Player 1'),
	('P2', 'Player 2'),
	('ND', 'Not Defined'),
	)
	C_Winner=models.CharField(verbose_name="Winner",max_length=2, choices=Choice, default='ND')
	D_RegisterDate=models.DateField( verbose_name="",auto_now_add=True, blank = True, null=True)

	def __str__(self):
		return u"%s VS %s" %(self.FK_player1.C_Name,self.FK_player2.C_Name)
	
	class Meta:
		verbose_name=u'Game'
		verbose_name_plural=u'Games'


class Round(models.Model):
	FK_Game = models.ForeignKey('Game', verbose_name="Game",blank=True,null=True,on_delete=models.CASCADE)
	Choice = (
	('P', 'PAPER'),
	('R', 'ROCK'),
	('S', 'SCISSORS'),
	('ND', 'Not Defined'),
	)
	ChoiceWinner = (
	('P1', 'Player 1'),
	('P2', 'Player 2'),
	('DW', 'Draw'),
	
	('ND', 'Not Defined'),
	)

	C_MoveP1=models.CharField(verbose_name="Move Player 1",max_length=1, choices=Choice, default='ND')
	C_MoveP2=models.CharField(verbose_name="Move Player 2",max_length=1, choices=Choice, default='ND')
	C_RoundWinner=models.CharField(verbose_name=" Round Winner",max_length=2, choices=ChoiceWinner, default='ND')
	D_RegisterDate=models.DateField( verbose_name="",auto_now_add=True, blank = True, null=True)

	def __str__(self):
		return u"%s  VS %s" %(self.C_MoveP1,self.C_MoveP2)
	
	class Meta:
		verbose_name=u'Round'
		verbose_name_plural=u'Rounds'
	


